function activarEstado1(){
    document.getElementById('circulo1').style.opacity='1';
    document.getElementById('circuloIcon1').style.opacity='1';
    document.getElementById("circuloParrafo1").innerHTML="En espera";
    setTimeout('activarEstado2()',2000);
}

function activarEstado2(){
    document.getElementById('circulo2').style.opacity='1';
    document.getElementById('circuloIcon2').style.opacity='1';
    document.getElementById('linea1').style.width='100%';
    document.getElementById("circuloParrafo2").innerHTML="En preparación";
    setTimeout('activarEstado3()',5000);
}
function activarEstado3(){
    document.getElementById('circulo3').style.opacity='1';
    document.getElementById('circuloIcon3').style.opacity='1';
    document.getElementById('linea2').style.width='100%';
    document.getElementById("circuloParrafo3").innerHTML="En ruta";
    setTimeout('activarEstado4()',5000);
}
function activarEstado4(){
    document.getElementById('circulo4').style.opacity='1';
    document.getElementById('circuloIcon4').style.opacity='1';
    document.getElementById('linea3').style.width='100%';
    document.getElementById("circuloParrafo4").innerHTML="Entregado!";
}
